import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class TagCloud extends Component {

  tagList = [
    {
      name: 'medical',
      link: '#medical'
    },
    {
      name: 'cure',
      link: '#cure'
    },
    {
      name: 'remedy',
      link: '#remedy'
    },
    {
      name: 'health',
      link: '#health'
    },
    {
      name: 'workout',
      link: '#workout'
    },
    {
      name: 'medicine',
      link: '#medicine'
    },
    {
      name: 'doctor',
      link: '#doctor'
    },
    {
      name: 'medic',
      link: '#medic'
    }
  ]

  tagItems = this.tagList.map((item) => <Link key={item.name} to={item.link} className="tag-cloud-link">{item.name}</Link>)

  render() {

    return (
      <div className="tagcloud">
        {this.tagItems}
      </div>
    )

  }
}