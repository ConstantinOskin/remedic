import React, { Component } from 'react';

export default class CardAdvantage extends Component {
  render() {

    const { title, text, link } = this.props;

    return (
      <a href={link} className="services-wrap">
        <div className="icon d-flex justify-content-center align-items-center">
          <span className="ion-ios-arrow-back"></span>
          <span className="ion-ios-arrow-forward"></span>
        </div>
        <h2>{title}</h2>
        <p>{text}</p>
      </a>
    )

  }
}