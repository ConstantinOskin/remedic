import React, { Component } from 'react';

import FormComment from '../form-comment';
import CardComment from '../card-comment';

export default class SectionComments extends Component {

  commentList = [
    {
      id: '1',
      name: 'John Doe',
      image: 'person_1.jpg',
      date: 'June 27, 2018',
      time: '2:21pm',
      text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quidem laborum necessitatibus, ipsam impedit vitae autem, eum officia, fugiat saepe enim sapiente iste iure! Quam voluptas earum impedit necessitatibus, nihil?'
    },
    {
      id: '2',
      name: 'John Doe',
      image: 'person_2.jpg',
      date: 'June 27, 2018',
      time: '2:21pm',
      text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quidem laborum necessitatibus, ipsam impedit vitae autem, eum officia, fugiat saepe enim sapiente iste iure! Quam voluptas earum impedit necessitatibus, nihil?'
    },
    {
      id: '3',
      name: 'John Doe',
      image: 'person_3.jpg',
      date: 'June 27, 2018',
      time: '2:21pm',
      text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quidem laborum necessitatibus, ipsam impedit vitae autem, eum officia, fugiat saepe enim sapiente iste iure! Quam voluptas earum impedit necessitatibus, nihil?'
    }
  ]

  commentItems = this.commentList.map((item) => {
    return <CardComment
      key={item.id}
      name={item.name}
      image={item.image}
      date={item.date}
      time={item.time}
      text={item.text} />
  })

  render() {

    return (
      <div className="pt-5 mt-5">
        <h3 className="mb-5">6 Comments</h3>
        <ul className="comment-list">
          {this.commentItems}
        </ul>
        <FormComment />
      </div>
    )

  }
}