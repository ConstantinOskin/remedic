import React, { Component } from 'react';

export default class TabDirection extends Component {

  setActiveElement = () => {
    return this.props.activeTab === this.props.id ? 'active' : '';
  }

  render() {

    const { id, name, icon, onClick } = this.props;

    return (
      <a className={`nav-link px-4 ${this.setActiveElement()}`}
        onClick={() => onClick(id)}
        id={`${id}-tab`}
        data-toggle="pill"
        href={`#${id}`}
        role="tab"
        aria-controls={id}
        aria-selected="false"><span className={`mr-3 flaticon-${icon}`}></span> {name}</a>
    )

  }
}