import React, { Component } from 'react';

export default class CardTest extends Component {

  state = {
    isVisible: false
  }

  chngeVisibleContent = () => {
    return this.state.isVisible ? 'show' : '';
  }

  changeIcon = () => {
    return this.state.isVisible ? '' : 'collapsed';
  }

  toggleContent = (e) => {
    e.preventDefault();
    if (this.state.isVisible) {
      this.setState({
        isVisible: false
      })
    } else {
      this.setState({
        isVisible: true
      })
    }
  }

  render() {

    const { title, price, text } = this.props;

    return (
      <div className="card">
        <div className="card-header">
          <a
            onClick={this.toggleContent}
            className={`card-link ${this.changeIcon()}`}
            data-toggle="collapse"
            href="#menuone"
            aria-expanded={this.state.isVisible}
            aria-controls="menuone">{title} <small>{price}</small><span className="collapsed"><i className="icon-plus-circle"></i></span><span className="expanded"><i className="icon-minus-circle"></i></span></a>
        </div>
        <div id="menuone" className={`collapse ${this.chngeVisibleContent()}`}>
          <div className="card-body">
            <p>{text}</p>
          </div>
        </div>
      </div>
    )
    
  }
}