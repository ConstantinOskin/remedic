import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Categories extends Component {

  categorieList = [
    {
      name: 'Departments',
      link: '#departments',
      count: '12'
    },
    {
      name: 'Doctors',
      link: '#doctors',
      count: '22'
    },
    {
      name: 'Medicine',
      link: '#medicine',
      count: '37'
    },
    {
      name: 'Hospital',
      link: '#hospital',
      count: '42'
    },
    {
      name: 'Cure',
      link: '#cure',
      count: '14'
    },
    {
      name: 'Health',
      link: '#health',
      count: '140'
    }
  ]

  categorieItems = this.categorieList.map((item) => {
    return <li key={item.name}>
      <Link to={item.link}>{item.name} <span>({item.count})</span></Link>
    </li>
  })

  render() {

    return (
      <div className="categories">
        <h3>Categories</h3>
        {this.categorieItems}
      </div>
    )

  }
}