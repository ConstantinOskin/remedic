import React, { Component } from 'react';
import { Map, InfoWindow, Marker, GoogleApiWrapper } from 'google-maps-react';

export class MapContainer extends Component {
  render() {

    const { google } = this.props;

    return (
      <div id="map">
        <Map
          style={{
            width: '100%',
            height: '100%',
            position: 'relative',
            "featureType": "administrative.country",
            "elementType": "geometry",
            "stylers": [
              {
                "visibility": "simplified"
              },
              {
                "hue": "#ff0000"
              }
            ]
          }}
          google={google}
          initialCenter={{
            lat: 40.69847032728747,
            lng: -73.9514422416687
          }}
          zoom={7}>

          <Marker
            onClick={this.onMarkerClick}
            name={'Current location'} />

          <InfoWindow
            onClose={this.onInfoWindowClose}>
            <div>
              <h1></h1>
            </div>
          </InfoWindow>
        </Map>
      </div>
    )

  }
}

export default GoogleApiWrapper({
  apiKey: ('AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s')
})(MapContainer)