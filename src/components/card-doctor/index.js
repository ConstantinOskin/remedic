import React, { Component } from 'react';

export default class CardDoctor extends Component {
  render() {

    const { name, position, blockquote, image } = this.props;

    const style = {
      backgroundImage: `url("/images/${image}")`
    }

    return (
      <div className="col-md-6 col-lg-3">
        <div className="block-2">
          <div className="flipper">
            <div className="front" style={style}>
              <div className="box">
                <h2>{name}</h2>
                <p>{position}</p>
              </div>
            </div>
            <div className="back">
              <blockquote>
                <p>{blockquote}</p>
              </blockquote>
              <div className="author d-flex">
                <div className="image mr-3 align-self-center">
                  <div className="img" style={style}></div>
                </div>
                <div className="name align-self-center">{name} <span className="position">{position}</span></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )

  }
}