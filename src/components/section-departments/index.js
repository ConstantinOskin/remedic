import React, { Component } from 'react';

import CardDepartment from '../card-department';

export default class SectionDepartments extends Component {

  departmentsList = [
    {
      id: 1,
      image: 'dept-1.jpg',
      name: 'Dental Department',
      address: '203 Fake St. California, USA',
      personal: '22 Doctors',
      descr: 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.',
      directions: [
        'Emergency', 'Laboratory', 'Dental'
      ]
    },
    {
      id: 2,
      image: 'dept-2.jpg',
      name: 'Surgical Department',
      address: '203 Fake St. California, USA',
      personal: '22 Doctors',
      descr: 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.',
      directions: [
        'Emergency', 'Laboratory', 'Dental'
      ]
    },
    {
      id: 3,
      image: 'dept-3.jpg',
      name: 'Neurological Department',
      address: '203 Fake St. California, USA',
      personal: '22 Doctors',
      descr: 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.',
      directions: [
        'Emergency', 'Laboratory', 'Dental'
      ]
    },
    {
      id: 4,
      image: 'dept-4.jpg',
      name: 'Ophthalmological Department',
      address: '203 Fake St. California, USA',
      personal: '22 Doctors',
      descr: 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.',
      directions: [
        'Emergency', 'Laboratory'
      ]
    }
  ]

  departmentItems = this.departmentsList.map((item) => {
    return <CardDepartment
      key={item.id}
      name={item.name}
      image={item.image}
      address={item.address}
      personal={item.personal}
      descr={item.descr}
      directions={item.directions} />
  })

  render() {
    
    return (
      <section className="ftco-section">
        <div className="container">
          <div className="row d-flex">
            {this.departmentItems}
          </div>
        </div>
      </section>
    )
    
  }
}