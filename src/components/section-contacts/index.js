import React, { Component } from 'react';

import FormContact from '../form-contact';
import GoogleApiWrapper from '../google-map';

export default class SectionContacts extends Component {
  
  contactInfo = {
    address: '198 West 21th Street, Suite 721 New York NY 10016',
    phone: '+79505679203',
    email: 'info@yoursite.com',
    site: 'yoursite.com'
  }

  render() {

    const { address, phone, email, site } = this.contactInfo;

    return (
      <section className="ftco-section contact-section ftco-degree-bg">
        <div className="container">
          <div className="row d-flex mb-5 contact-info">
            <div className="col-md-12 mb-4">
              <h2 className="h4">Contact Information</h2>
            </div>
            <div className="w-100"></div>
            <div className="col-md-3"
              ref={this.addressRef}>
              <p><span>Address:</span> {address}</p>
            </div>
            <div className="col-md-3">
              <p><span>Phone:</span> <a href="tel://1234567920">{phone}</a></p>
            </div>
            <div className="col-md-3">
              <p><span>Email:</span> <a href="mailto:info@yoursite.com">{email}</a></p>
            </div>
            <div className="col-md-3">
              <p><span>Website</span> <a href="yoursite.com">{site}</a></p>
            </div>
          </div>
          <div className="row block-9">
            <div className="col-md-6 pr-md-5">
              <FormContact />
            </div>
            <div className="col-md-6">
              <GoogleApiWrapper />
            </div>
          </div>
        </div>
      </section>
    )

  }
}