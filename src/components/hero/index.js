import React, { Component } from 'react';

import { Animated } from 'react-animated-css';

export default class Hero extends Component {
  render() {

    const { title, text } = this.props;

    const style = {
      backgroundImage: 'url("/images/bg_1.jpg")',
      backgroundAttachment: 'fixed'
    }

    const ShowTitle = () => {
      return title !== '' ? <h1 className="mb-4">{title}</h1> : '';
    }

    const ShowText = () => {
      return text !== undefined ? <p>{text}</p> : '';
    }

    return (
      <div className="hero-wrap" style={style}>
        <div className="overlay"></div>
        <div className="container">
          <div className="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true">
            <Animated className="col-md-8 text-center">
              <ShowTitle />
              <ShowText />
            </Animated>
          </div>
        </div>
      </div>
    )
    
  }
}