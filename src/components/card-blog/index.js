import React, { Component } from 'react';

export default class CardBlog extends Component {
  render() {

    const { link, title, text, image, date, author, commentCount } = this.props;

    const style = {
      backgroundImage: `url("/images/${image}")`
    }

    return (
      <div className="col-md-6">
        <div className="blog-entry align-self-stretch d-flex">
          <a href={link} className="block-20 order-md-last" style={style}>
          </a>
          <div className="text p-4 d-block">
            <div className="meta mb-3">
              <div><a href={date}>{date}</a></div>
              <div><a href={author}>{author}</a></div>
              <div><a href={commentCount} className="meta-chat"><span className="icon-chat"></span> {commentCount}</a></div>
            </div>
            <h3 className="heading mt-3"><a href={link}>{title}</a></h3>
            <p>{text}</p>
          </div>
        </div>
      </div>
    )

  }
}