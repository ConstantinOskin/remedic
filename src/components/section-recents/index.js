import React, { Component } from 'react';

import CardBlog from '../card-blog';

export default class SectionRecents extends Component {

  blogsList = [
    {
      id: 1,
      link: 'index',
      title: 'New technology facilities',
      text: 'A small river named Duden flows by their place and supplies it with the necessary regelialia.',
      image: 'image_1.jpg',
      date: 'August 12, 2018',
      author: 'Admin',
      commentCount: '3'
    },
    {
      id: 2,
      link: 'index',
      title: 'New technology facilities',
      text: 'A small river named Duden flows by their place and supplies it with the necessary regelialia.',
      image: 'image_2.jpg',
      date: 'August 12, 2018',
      author: 'Admin',
      commentCount: '3'
    },
    {
      id: 3,
      link: 'index',
      title: 'New technology facilities',
      text: 'A small river named Duden flows by their place and supplies it with the necessary regelialia.',
      image: 'image_3.jpg',
      date: 'August 12, 2018',
      author: 'Admin',
      commentCount: '3'
    },
    {
      id: 4,
      link: 'index',
      title: 'New technology facilities',
      text: 'A small river named Duden flows by their place and supplies it with the necessary regelialia.',
      image: 'image_4.jpg',
      date: 'August 12, 2018',
      author: 'Admin',
      commentCount: '3'
    },
    {
      id: 5,
      link: 'index',
      title: 'New technology facilities',
      text: 'A small river named Duden flows by their place and supplies it with the necessary regelialia.',
      image: 'image_5.jpg',
      date: 'August 12, 2018',
      author: 'Admin',
      commentCount: '3'
    },
    {
      id: 6,
      link: 'index',
      title: 'New technology facilities',
      text: 'A small river named Duden flows by their place and supplies it with the necessary regelialia.',
      image: 'image_6.jpg',
      date: 'August 12, 2018',
      author: 'Admin',
      commentCount: '3'
    },
  ]

  blogItems = this.blogsList.map((item) => {
    return <CardBlog
      key={item.id}
      link={item.link}
      title={item.title}
      text={item.text}
      image={item.image}
      date={item.date}
      author={item.author}
      commentCount={item.commentCount} />
  })

  render() {

    const { title } = this.props;

    const ShowTitle = () => {
      return title !== undefined ? <div className="row justify-content-center mb-5 pb-3">
        <div className="col-md-7 heading-section text-center">
          <h2 className="mb-4">{title}</h2>
        </div>
      </div> : '';
    }

    return (
      <section className="ftco-section bg-light">
        <div className="container">
          <ShowTitle />
          <div className="row d-flex">
            {this.blogItems}
          </div>
        </div>
      </section>
    )

  }
}
