import React from 'react';
import { Link } from "react-router-dom";

const Navbar = () => {

  return (
    <nav className="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
      <div className="container">
        <a className="navbar-brand" href="index.html"><i className="flaticon-pharmacy"></i><span>Re</span>Medic</a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
          <span className="oi oi-menu"></span> Menu
      </button>
        <div className="collapse navbar-collapse" id="ftco-nav">
          <ul className="navbar-nav ml-auto">
            <li className="nav-item active">
              <Link className="nav-link" to="/">Home</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/about/">About</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/departments/">Departments</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/doctors/">Doctors</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/blog/">Blog</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/contact/">Contact</Link>
            </li>
            <li className="nav-item cta">
              <a href="contact.html" className="nav-link" data-toggle="modal" data-target="#modalAppointment"><span>Make an Appointment</span></a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
  
}

export default Navbar;