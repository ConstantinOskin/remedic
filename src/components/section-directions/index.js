import React, { Component } from 'react';

import CardDirection from '../card-direction';
import TabDirection from '../tab-direction';

export default class SectionDirections extends Component {

  state = {
    activeTab: 'v-pills-master'
  }

  changeTab = (value) => {
    this.setState({
      activeTab: value
    })
  }

  directionsList = [
    {
      id: 'v-pills-master',
      name: 'Cardiology',
      title: 'Cardiology Department',
      descr: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt iste dolores consequatur',
      text: 'Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis officiis nobis sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae deserunt qui nihil ea, tempore et quam natus quaerat doloremque.',
      link: '1',
      icon: 'cardiogram'
    },
    {
      id: 'v-pills-buffet',
      name: 'Neurology',
      title: 'Neurogoly Department',
      descr: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt iste dolores consequatur',
      text: 'Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis officiis nobis sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae deserunt qui nihil ea, tempore et quam natus quaerat doloremque.',
      link: '1',
      icon: 'neurology'
    },
    {
      id: 'v-pills-fitness',
      name: 'Diagnostic',
      title: 'Diagnostic Department',
      descr: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt iste dolores consequatur',
      text: 'Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis officiis nobis sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae deserunt qui nihil ea, tempore et quam natus quaerat doloremque.',
      link: '1',
      icon: 'stethoscope'
    },
    {
      id: 'v-pills-reception',
      name: 'Dental',
      title: 'Dental Departments',
      descr: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt iste dolores consequatur',
      text: 'Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis officiis nobis sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae deserunt qui nihil ea, tempore et quam natus quaerat doloremque.',
      link: '1',
      icon: 'tooth'
    },
    {
      id: 'v-pills-sea',
      name: 'Ophthalmology',
      title: 'Ophthalmology Departments',
      descr: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt iste dolores consequatur',
      text: 'Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis officiis nobis sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae deserunt qui nihil ea, tempore et quam natus quaerat doloremque.',
      link: '1',
      icon: 'vision'
    },
    {
      id: 'v-pills-spa',
      name: 'Emergency',
      title: 'Emergency Departments',
      descr: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt iste dolores consequatur',
      text: 'Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis officiis nobis sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae deserunt qui nihil ea, tempore et quam natus quaerat doloremque.',
      link: '1',
      icon: 'ambulance'
    }
  ]

  render() {

    const directionItems = this.directionsList.map((item) => {
      return <CardDirection
        key={item.id}
        id={item.id}
        title={item.title}
        descr={item.descr}
        text={item.text}
        link={item.link}
        icon={item.icon}
        activeTab={this.state.activeTab} />
    })

    const directionTabs = this.directionsList.map((item) => {
      return <TabDirection
        onClick={this.changeTab}
        key={item.id}
        id={item.id}
        name={item.name}
        icon={item.icon}
        activeTab={this.state.activeTab} />
    })

    return (
      <section className="ftco-services">
        <div className="container">
          <div className="row no-gutters">
            <div className="col-md-4 py-5 nav-link-wrap">
              <div className="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                {directionTabs}
              </div>
            </div>
            <div className="col-md-8 p-4 p-md-5 d-flex align-items-center">
              <div className="tab-content pl-md-5" id="v-pills-tabContent">
                {directionItems}
              </div>
            </div>
          </div>
        </div>
      </section>
    )

  }
}