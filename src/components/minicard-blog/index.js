import React, { Component } from 'react';

export default class MinicardBlog extends Component {
  render() {

    const { title, image, date, author, commentCount, link } = this.props;

    const style = {
      backgroundImage: `url("/images/${image}")`
    }

    return (
      <div className="block-21 mb-4 d-flex">
        <a className="blog-img mr-4" style={style}></a>
        <div className="text">
          <h3 className="heading"><a href={link}>{title}</a>
          </h3>
          <div className="meta">
            <div><a href="#"><span className="icon-calendar"></span> {date}</a></div>
            <div><a href="#"><span className="icon-person"></span> {author}</a></div>
            <div><a href="#"><span className="icon-chat"></span> {commentCount}</a></div>
          </div>
        </div>
      </div>
    )

  }
}