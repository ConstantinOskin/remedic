import React, { Component } from 'react';

export default class FormContact extends Component {

  state = {
    name: '',
    email: '',
    subject: '',
    message: '',
    errors: {
      name: false,
      email: false,
      subject: false,
      message: false
    },
    valid: false
  }

  validEmailRegex = RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);

  changeInput = (e) => {
    e.preventDefault();
    const { name, value } = e.target;
    let errors = this.state.errors;
    this.setState({
      [name]: value
    })
    switch (name) {
      case 'name':
        errors.name = value.length < 5 ? false : true;
        break;
      case 'email':
        errors.email = this.validEmailRegex.test(value) ? true : false;
        break;
      case 'subject':
        errors.subject = value.length < 5 ? false : true;
        break;
      case 'message':
        errors.message = value.length < 5 ? false : true;
        break;
      default:
        break;
    }
    this.validateForm();
  }

  validateForm = () => {
    let invalidLength = Object.values(this.state.errors).filter((item) => item === false).length;
    return invalidLength === 0 ? this.setState({ valid: true }) : this.setState({ valid: false });
  }

  isValidForm = () => this.state.valid ? 'active' : '';

  render() {

    return (
      <form action="#">
        <div className="form-group">
          <input type="text"
            className="form-control"
            placeholder="Your Name"
            value={this.state.name}
            name="name"
            onChange={this.changeInput} />
        </div>
        <div className="form-group">
          <input type="text"
            className="form-control"
            placeholder="Your Email"
            value={this.state.email}
            name="email"
            onChange={this.changeInput} />
        </div>
        <div className="form-group">
          <input type="text"
            className="form-control"
            placeholder="Subject"
            value={this.state.subject}
            name="subject"
            onChange={this.changeInput} />
        </div>
        <div className="form-group">
          <textarea name="" id="" cols="30" rows="7"
            className="form-control"
            placeholder="Message"
            value={this.state.message}
            name="message"
            onChange={this.changeInput}></textarea>
        </div>
        <div className="form-group">
          <input type="submit" value="Send Message" className={`btn btn-primary py-3 px-5 ${this.isValidForm()}`} />
        </div>
      </form>
    )
    
  }
}