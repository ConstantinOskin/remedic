import React, { Component } from 'react';

export default class CardFact extends Component {

  state = {
    counter: 0
  }

  changeCounter() {
    if (this.state.counter < this.props.maxCount) {
      this.setState({
        counter: this.state.counter + 1
      })
    }
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.changeCounter(),
      5000 / this.props.maxCount
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }


  render() {

    const { name } = this.props;

    return (
      <div className="col-md-3 d-flex justify-content-center counter-wrap">
        <div className="block-18 text-center">
          <div className="text">
            <strong className="number">{this.state.counter}</strong>
            <span>{name}</span>
          </div>
        </div>
      </div>
    )
    
  }
}