import React, { Component } from 'react';

import CardDoctor from '../card-doctor';

export default class SectionDoctors extends Component {

  doctorsList = [
    {
      id: 1,
      name: 'Aldin Powell',
      position: 'Neurologist',
      blockquote: 'Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem',
      image: 'doctor-1.jpg'
    },
    {
      id: 2,
      name: 'Aldin Powell',
      position: 'Pediatrician',
      blockquote: 'Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem',
      image: 'doctor-2.jpg'
    },
    {
      id: 3,
      name: 'Aldin Powell',
      position: 'Ophthalmologist',
      blockquote: 'Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem',
      image: 'doctor-3.jpg'
    },
    {
      id: 4,
      name: 'Aldin Powell',
      position: 'Pulmonologist',
      blockquote: 'Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem',
      image: 'doctor-4.jpg'
    },
  ]

  doctorItems = this.doctorsList.map((item) => {
    return <CardDoctor
      key={item.id}
      name={item.name}
      position={item.position}
      blockquote={item.blockquote}
      image={item.image} />
  })

  render() {

    const { title, descrTitle, descrText } = this.props;

    const ShowTitle = () => {
      return title !== undefined ? <div className="row justify-content-center mb-5 pb-3">
        <div className="col-md-7 heading-section text-center">
          <h2 className="mb-4">{title}</h2>
        </div>
      </div> : '';
    }

    const ShowDescr = () => {
      if (descrTitle !== undefined && descrText !== undefined) {
        return <div className="row">
          <div className="col-md-9">
            <h4>{descrTitle}</h4>
            <p>{descrText}</p>
          </div>
        </div>
      }
      else if (descrTitle !== undefined && descrText === undefined) {
        return <div className="row">
          <div className="col-md-9">
            <h4>{descrTitle}</h4>
          </div>
        </div>
      }
      else if (descrTitle === undefined && descrText !== undefined) {
        return <div className="row">
          <div className="col-md-9">
            <p>{descrText}</p>
          </div>
        </div>
      } else {
        return '';
      }
    }

    return (
      <section className="ftco-section">
        <div className="container">
          <ShowTitle />
          <div className="row">
            {this.doctorItems}
          </div>
          <ShowDescr />
        </div>
      </section>
    )

  }
}