import React, { Component } from 'react';

export default class CardAuthor extends Component {

  authorInfo = {
    name: 'Lance Smith',
    text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur similique, inventore eos fugit cupiditate numquam!',
    avatar: 'person_1.jpg'
  }

  render() {

    const { name, text, avatar } = this.authorInfo;

    return (
      <div className="about-author d-flex p-4 bg-light">
        <div className="bio align-self-md-center mr-5">
          <img
            src={`/images/${avatar}`}
            alt="Image placeholder"
            className="img-fluid mb-4" />
        </div>
        <div className="desc align-self-md-center">
          <h3>{name}</h3>
          <p>{text}</p>
        </div>
      </div>
    )

  }
}