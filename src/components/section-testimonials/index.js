import React, { Component } from 'react';

import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';

import CardTestimonial from '../card-testimonial';

export default class SectionTestimonials extends Component {

  testimonialsList = [
    {
      id: 1,
      image: 'person_1.jpg',
      name: 'Dennis Green',
      text: 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.',
      position: 'Patient'
    },
    {
      id: 2,
      image: 'person_2.jpg',
      name: 'Dennis Green',
      text: 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.',
      position: 'Doctor'
    },
    {
      id: 3,
      image: 'person_3.jpg',
      name: 'Dennis Green',
      text: 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.',
      position: 'Patient'
    },
    {
      id: 4,
      image: 'person_1.jpg',
      name: 'Dennis Green',
      text: 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.',
      position: 'Doctor'
    },
  ]

  testimonialItems = this.testimonialsList.map((item) => {
    return <CardTestimonial
      key={item.id}
      name={item.name}
      image={item.image}
      text={item.text}
      position={item.position} />
  })

  render() {

    return (
      <section className="ftco-section testimony-section">
        <div className="container">
          <div className="row justify-content-center mb-5 pb-3">
            <div className="col-md-7 heading-section text-center">
              <h2 className="mb-4">Testimonials</h2>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <OwlCarousel
                className="carousel-testimony owl-carousel"
                loop
                margin={10}
                nav>
                {this.testimonialItems}
              </OwlCarousel>
            </div>
          </div>
        </div>
      </section>
    )

  }
}