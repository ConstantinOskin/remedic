import React, { Component } from 'react';

import CardTest from '../card-test';

export default class SectionTests extends Component {

  testsList = [
    {
      id: 1,
      title: 'Aldin Powell',
      price: '$99.00',
      text: 'When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane. Pityful a rethoric question ran over her cheek, then she continued her way.'
    },
    {
      id: 2,
      title: 'Aldin Powell',
      price: '$99.00',
      text: 'When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane. Pityful a rethoric question ran over her cheek, then she continued her way.'
    }
  ]

  testItems = this.testsList.map((item) => {
    return <CardTest
      key={item.id}
      title={item.title}
      price={item.price}
      text={item.text} />
  })

  render() {

    const { title } = this.props;

    const ShowTitle = () => {
      return title !== undefined ? <div className="row justify-content-center mb-5 pb-3">
        <div className="col-md-7 heading-section text-center">
          <h2 className="mb-4">{title}</h2>
        </div>
      </div> : '';
    }

    return (
      <section className="ftco-section bg-light">
        <div className="container">
          <ShowTitle />

          <div className="row">
            <div className="col-md-12">
              <div id="accordion">
                <div className="row">
                  <div className="col-md-6">
                    {this.testItems}
                  </div>
                  <div className="col-md-6">
                    {this.testItems}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    )

  }
}