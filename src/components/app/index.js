import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";

import Navbar from '../navbar';
import SectionSubscribes from '../section-subscribes';
import Footer from '../footer';
import Home from '../../templates/home';
import About from '../../templates/about';
import Departments from '../../templates/departments';
import Doctors from '../../templates/doctors';
import Blog from '../../templates/blog';
import BlogDetails from '../../templates/blog-deteils';
import Contact from '../../templates/contact';
import Appointments from '../../templates/appointments';
import OurSpecialties from '../../templates/our-specialties';
import WhyChooseUs from '../../templates/why-choose-us';
import OurServices from '../../templates/our-services';
import HealthTips from '../../templates/health-tips';


export default class App extends Component {
  render() {

    return (
      <div className="App">
        <Router>
          <Navbar />

          <Route exact path="/" component={Home} />
          <Route path="/about/" component={About} />
          <Route path="/departments/" component={Departments} />
          <Route path="/doctors/" component={Doctors} />
          <Route path="/blog/" component={Blog} />
          <Route path="/contact/" component={Contact} />
          <Route path="/appointments/" component={Appointments} />
          <Route path="/our-specialties/" component={OurSpecialties} />
          <Route path="/why-choose-us/" component={WhyChooseUs} />
          <Route path="/our-services/" component={OurServices} />
          <Route path="/health-tips/" component={HealthTips} />

          <SectionSubscribes />
          <Footer />
        </Router>
      </div>
    )

  }
}