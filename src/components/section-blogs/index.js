import React, { Component } from 'react';

import MinicardBlog from '../minicard-blog';

export default class SectionBlogs extends Component {

  blogList = [
    {
      id: '1',
      image: 'image_1.jpg',
      title: 'Even the all-powerful Pointing has no control about the blind texts',
      link: '#one',
      date: 'July 12, 2018',
      author: 'Admin',
      commentCount: '19'
    },
    {
      id: '2',
      image: 'image_2.jpg',
      title: 'Even the all-powerful Pointing has no control about the blind texts',
      link: '#one',
      date: 'July 12, 2018',
      author: 'Admin',
      commentCount: '29'
    },
    {
      id: '3',
      image: 'image_3.jpg',
      title: 'Even the all-powerful Pointing has no control about the blind texts',
      link: '#one',
      date: 'July 12, 2018',
      author: 'Admin',
      commentCount: '119'
    }
  ]

  blogItems = this.blogList.map((item) => {
    return <MinicardBlog
      key={item.id}
      image={item.image}
      title={item.title}
      link={item.link}
      date={item.date}
      author={item.author}
      commentCount={item.commentCount} />
  })

  render() {

    return (
      <div>
        <h3>Recent Blog</h3>
        {this.blogItems}
      </div>
    )

  }
}