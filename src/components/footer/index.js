import React, { Component } from 'react';
import { Link } from "react-router-dom";

export default class Footer extends Component {
  render() {

    const style = {
      backgroundImage: 'url("/images/bg_5.jpg")'
    }

    return (
      <footer className="ftco-footer ftco-bg-dark ftco-section img" style={style}>
        <div className="overlay"></div>
        <div className="container">
          <div className="row mb-5">
            <div className="col-md">
              <div className="ftco-footer-widget mb-4">
                <h2 className="ftco-heading-2">Remedic</h2>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                <ul className="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                  <li className="ftco-animate"><a href="1"><span className="icon-twitter"></span></a></li>
                  <li className="ftco-animate"><a href="1"><span className="icon-facebook"></span></a></li>
                  <li className="ftco-animate"><a href="1"><span className="icon-instagram"></span></a></li>
                </ul>
              </div>
            </div>
            <div className="col-md">
              <div className="ftco-footer-widget mb-4 ml-md-5">
                <h2 className="ftco-heading-2">Information</h2>
                <ul className="list-unstyled">
                  <li>
                    <Link className="py-2 d-block" to="/appointments/">Appointments</Link>
                  </li>
                  <li>
                    <Link className="py-2 d-block" to="/our-specialties/">Our Specialties</Link>
                  </li>
                  <li>
                    <Link className="py-2 d-block" to="/why-choose-us/">Why Choose us</Link>
                  </li>
                  <li>
                    <Link className="py-2 d-block" to="/our-services">Our Services</Link>
                  </li>
                  <li>
                    <Link className="py-2 d-block" to="/health-tips">health Tips</Link>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-md">
              <div className="ftco-footer-widget mb-4">
                <h2 className="ftco-heading-2">Site Links</h2>
                <ul className="list-unstyled">
                  <li>
                    <Link className="py-2 d-block" to="/">Home</Link>
                  </li>
                  <li>
                    <Link className="py-2 d-block" to="/about/">About</Link>
                  </li>
                  <li>
                    <Link className="py-2 d-block" to="/departments/">Departments</Link>
                  </li>
                  <li>
                    <Link className="py-2 d-block" to="/doctors/">Doctors</Link>
                  </li>
                  <li>
                    <Link className="py-2 d-block" to="/blog/">Blog</Link>
                  </li>
                  <li>
                    <Link className="py-2 d-block" to="/contact/">Contact</Link>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-md">
              <div className="ftco-footer-widget mb-4">
                <h2 className="ftco-heading-2">Have a Questions?</h2>
                <div className="block-23 mb-3">
                  <ul>
                    <li><span className="icon icon-map-marker"></span><span className="text">203 Fake St. Mountain View, San Francisco, California, USA</span></li>
                    <li><a href="1"><span className="icon icon-phone"></span><span className="text">+2 392 3929 210</span></a></li>
                    <li><a href="1"><span className="icon icon-envelope"></span><span className="text">info@yourdomain.com</span></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
    )
    
  }
}