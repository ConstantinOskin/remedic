import React, { Component } from 'react';

export default class CardDepartment extends Component {

  directionItems = this.props.directions.map((item) => {
    return <li key={item}><span className="ion-ios-checkmark"></span>{item}</li>
  })

  render() {

    const { image, name, address, personal, descr } = this.props;

    const style = {
      backgroundImage: `url("/images/${image}")`
    }

    return (
      <div className="col-lg-6 d-flex">
        <div className="dept d-md-flex">
          <a href={name} className="img" style={style}> </a>
          <div className="text p-4">
            <h3><a href={name}>{name}</a></h3>
            <p><span className="loc">{address}</span></p>
            <p><span className="doc">{personal}</span></p>
            <p>{descr}</p>
            <ul>
              {this.directionItems}
            </ul>
          </div>
        </div>
      </div>
    )

  }
}