import React, { Component } from 'react';

export default class CardDirection extends Component {

  setActiveElement = () => {
    return this.props.activeTab === this.props.id ? 'show active' : '';
  }

  render() {

    const { id, title, descr, text, link, icon } = this.props;

    return (
      <div className={`tab-pane fade py-5 ${this.setActiveElement()}`} id={`#${id}`} role="tabpanel" aria-labelledby={`${id}-tab`}>
        <span className={`icon mb-3 d-block flaticon-${icon}`}></span>
        <h2 className="mb-4">{title}</h2>
        <p>{descr}</p>
        <p>{text}</p>
        <p><a href={link} className="btn btn-primary">Learn More</a></p>
      </div>
    )

  }
}