import React, { Component } from 'react';

import CardAdvantage from '../card-advantage';

export default class SectionAdvantages extends Component {

  advantagesList = [
    {
      id: 1,
      title: 'Laboratory Services',
      text: 'Even the all-powerful Pointing has no control about the blind.',
      link: 'link'
    },
    {
      id: 2,
      title: 'General Treatment',
      text: 'Even the all-powerful Pointing has no control about the blind.',
      link: 'link'
    },
    {
      id: 3,
      title: 'Emergency Service',
      text: 'Even the all-powerful Pointing has no control about the blind.',
      link: 'link'
    },
    {
      id: 4,
      title: '24/7 Help &amp; Support',
      text: 'Even the all-powerful Pointing has no control about the blind.',
      link: 'link'
    }
  ]

  advantageItems = this.advantagesList.map((item) => {
    return <CardAdvantage
      key={item.id}
      title={item.title}
      text={item.text}
      link={item.link} />
  })

  render() {

    const style = {
      backgroundImage: `url("/images/bg_3.jpg")`
    }

    return (
      <section className="ftco-section-2 img" style={style}>
        <div className="container">
          <div className="row d-md-flex justify-content-end">
            <div className="col-md-6">
              <div className="row">
                <div className="col-md-12">
                  {this.advantageItems}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
    
  }
}