import React, { Component } from 'react';

export default class CardTestimonial extends Component {
  render() {

    const { image, text, name, position } = this.props;

    const style = {
      backgroundImage: `url(/images/${image})`
    }

    return (
      <div className="item">
        <div className="testimony-wrap text-center">
          <div className="user-img mb-5" style={style}>
            <span className="quote d-flex align-items-center justify-content-center">
              <i className="icon-quote-left"></i>
            </span>
          </div>
          <div className="text">
            <p className="mb-5">{text}</p>
            <p className="name">{name}</p>
            <span className="position">{position}</span>
          </div>
        </div>
      </div>
    )
    
  }
}