import React, { Component } from 'react';

export default class CardComment extends Component {
  render() {

    const { name, image, date, time, text, } = this.props;

    return (
      <li className="comment">
        <div className="vcard bio">
          <img src={`/images/${image}`} alt="Image placeholder" />
        </div>
        <div className="comment-body">
          <h3>{name}</h3>
          <div className="meta">{date} at {time}</div>
          <p>{text}</p>
          <p><a href="#" className="reply">Reply</a></p>
        </div>
      </li>
    )

  }
}