import React, { Component } from 'react';

import TagCloud from '../tag-cloud';
import Categories from '../categories';
import SectionBlogs from '../section-blogs';
import Seo from '../seo';
import FormSearch from '../form-search';

export default class Aside extends Component {
  render() {

    return (
      <div className="col-md-4 sidebar">
        <div className="sidebar-box">
          <FormSearch />
        </div>
        <div className="sidebar-box">
          <Categories />
        </div>

        <div className="sidebar-box">
          <SectionBlogs />
        </div>

        <div className="sidebar-box">
          <h3>Tag Cloud</h3>
          <TagCloud />
        </div>

        <div className="sidebar-box">
          <Seo />
        </div>
      </div>
    )

  }
}