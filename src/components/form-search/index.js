import React from 'react';

const FormSearch = () => {
  return (
    <form action="#" className="search-form">
      <div className="form-group">
        <span className="icon fa fa-search"></span>
        <input type="text" className="form-control" placeholder="Type a keyword and hit enter" />
      </div>
    </form>
  )
}

export default FormSearch;