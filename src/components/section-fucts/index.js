import React, { Component } from 'react';

import CardFact from '../card-fact';

export default class SectionFucts extends Component {

  factsList = [
    {
      id: 1,
      name: 'Hospital',
      maxCount: '60'
    },
    {
      id: 2,
      name: 'Doctors',
      maxCount: '200'
    },
    {
      id: 3,
      name: 'Clinics',
      maxCount: '100'
    },
    {
      id: 4,
      name: 'Reviews',
      maxCount: '200'
    },
  ]

  factItems = this.factsList.map((item) => {
    return <CardFact
      key={item.id}
      name={item.name}
      maxCount={item.maxCount} />
  })

  render() {

    const style = {
      backgroundImage: `url("images/bg_4.jpg")`
    }

    return (
      <section className="ftco-section ftco-counter img" style={style} id="section-counter">
        <div className="container">
          <div className="row justify-content-center mb-5 pb-3">
            <div className="col-md-7 text-center heading-section heading-section-white">
              <h2 className="mb-4">Some fun facts</h2>
              <span className="subheading">Far far away, behind the word mountains, far from the countries</span>
            </div>
          </div>
          <div className="row justify-content-center">
            <div className="col-md-10">
              <div className="row">
                {this.factItems}
              </div>
            </div>
          </div>
        </div>
      </section>
    )

  }
}