import React, { Component } from 'react';

import Hero from '../../components/hero';
import SectionDirections from '../../components/section-directions';
import SectionAdvantages from '../../components/section-advantages';
import SectionDoctors from '../../components/section-doctors';
import SectionFucts from '../../components/section-fucts';
import SectionTestimonials from '../../components/section-testimonials';
import SectionRecents from '../../components/section-recents';

export default class Home extends Component {
  render() {

    return (
      <div>
        <Hero
          title="The most valuable thing is your Health"
          text="We care about your health Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life" />
        <SectionDirections />
        <SectionAdvantages />
        <SectionDoctors />
        <SectionFucts />
        <SectionTestimonials />
        <SectionRecents />
      </div>
    )
    
  }
}