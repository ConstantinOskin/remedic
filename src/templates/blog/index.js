import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import Hero from '../../components/hero';
import SectionRecents from '../../components/section-recents';
import BlogDetails from '../blog-deteils';

export default class About extends Component {
  render() {

    return (
      <div>
        <Hero
          title="Blog" />
        <Route exact path="/blog/"
          render={() => {
            return <SectionRecents
              title="Recent from blog" />
          }} />
        <Route path="/blog/:id"
          render={({ match }) => {
            const id = match.params;
            return <BlogDetails itemId={id} />
          }} />
      </div>
    )

  }
}