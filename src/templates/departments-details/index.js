import React, { Component } from 'react';

import SectionDoctors from '../../components/section-doctors';
import Aside from '../../components/aside';
import CardAutor from '../../components/card-author';
import TagCloud from '../../components/tag-cloud';
import Article from '../../components/article';

export default class DepartmentsDetails extends Component {
  render() {
    return (
      <section className="ftco-section ftco-degree-bg">
        <div className="container">
          <div className="row">
            <div className="col-md-8">
              <Article />
              <div className="tag-widget post-tag-container mb-5 mt-5">
                <TagCloud />
              </div>
              <CardAutor />
              <SectionDoctors
                title="OUR EXPERIENCED DOCTORS" />
            </div>
            <Aside />
          </div>
        </div>
      </section>
    )
  }
}