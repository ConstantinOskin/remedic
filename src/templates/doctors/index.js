import React, { Component } from 'react';

import Hero from '../../components/hero';
import SectionDoctors from '../../components/section-doctors';

export default class About extends Component {
  render() {

    return (
      <div>
        <Hero
          title="Well Experienced Doctors" />
        <SectionDoctors
          descrTitle="We are well experienced doctors"
          descrText="A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth." />
      </div>
    )
    
  }
}