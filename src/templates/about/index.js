import React, { Component } from 'react';

import Hero from '../../components/hero';
import SectionAdvantages from '../../components/section-advantages';
import SectionDoctors from '../../components/section-doctors';
import SectionFucts from '../../components/section-fucts';
import SectionTestimonials from '../../components/section-testimonials';
import SectionWelcoms from '../../components/section-welcoms';

export default class About extends Component {
  render() {

    return (
      <div>
        <Hero
          title="About Us" />
        <SectionWelcoms />
        <SectionDoctors
          title="OUR EXPERIENCED DOCTORS"
          descrTitle="We are well experienced doctors"
          descrText="A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth." />
        <SectionAdvantages />
        <SectionFucts />
        <SectionTestimonials />
      </div>
    )
    
  }
}