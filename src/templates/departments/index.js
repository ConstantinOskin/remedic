import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import Hero from '../../components/hero';
import SectionTests from '../../components/section-tests';
import SectionDepartments from '../../components/section-departments';
import SectionDoctors from '../../components/section-doctors';
import DepartmentsDetails from '../departments-details';

export default class About extends Component {
  render() {

    return (
      <div>
        <Hero
          title="Departments" />
        <Route exact path="/departments/"
          render={() => {
            return (
              <div>
                <SectionDepartments />
                <SectionTests
                  title="Laboratory Test" />
                <SectionDoctors
                  title="OUR EXPERIENCED DOCTORS" />
              </div>
            )
          }} />
        <Route path="/departments/:id"
          render={({ match }) => {
            const id = match.params;
            return <DepartmentsDetails itemId={id} />
          }} />
      </div>
    )

  }
}