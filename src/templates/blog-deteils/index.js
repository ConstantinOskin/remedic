import React, { Component } from 'react';

import Aside from '../../components/aside';
import CardAutor from '../../components/card-author';
import TagCloud from '../../components/tag-cloud';
import Article from '../../components/article';
import SectionComments from '../../components/section-comments';

export default class BlogDetails extends Component {
  render() {
    return (
      <section className="ftco-section ftco-degree-bg">
        <div className="container">
          <div className="row">
            <div className="col-md-8">
              <Article />
              <div className="tag-widget post-tag-container mb-5 mt-5">
                <TagCloud />
              </div>
              <CardAutor />
              <SectionComments />
            </div>
            <Aside />
          </div>
        </div>
      </section>
    )
  }
}