import React, { Component } from 'react';

import Hero from '../../components/hero';
import SectionContacts from '../../components/section-contacts';

export default class About extends Component {
  render() {

    return (
      <div>
        <Hero
          title="Contact Us" />
        <SectionContacts />
      </div>
    )
    
  }
}